package com.timbuchalka;

import java.util.*;

public class CustomClass<T> {

    private final Set<T> treeSet = new TreeSet<T>();

    // Add Item
    public void addItem(T obj) {
        try {
            validateDuplicate((Integer) obj);
            treeSet.add(obj);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    // customException validateDuplicate
    void validateDuplicate(int values) throws CustomException {
        for (T check : treeSet) {
            if (check.equals(values)){
                throw new CustomException("Duplicate Values " + values);
            }
        }
    }

    //   Get Item
    public Set<T> getTreeSet() {
        return treeSet;
    }
}

