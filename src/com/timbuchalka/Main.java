package com.timbuchalka;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        CustomClass<Integer> integerCustomClass = new CustomClass<>();
        try {
            integerCustomClass.addItem(1);
            integerCustomClass.addItem(14);
            integerCustomClass.addItem(1);
            integerCustomClass.addItem(10);
            integerCustomClass.addItem(null);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }

        System.out.println("\nList all from inputted");
        for (Integer element : integerCustomClass.getTreeSet()) {
            System.out.println(element);
        }
    }
}
